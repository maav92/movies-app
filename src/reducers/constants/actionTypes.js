// Movies actions
export const GET_ALL_MOVIES = 'GET_ALL_MOVIES';
export const SELECT_MOVIE = 'SELECT_MOVIE';
export const ERROR_MOVIES = 'ERROR_MOVIES';
export const LOADING_MOVIES = 'LOADING_MOVIES';
export const ADD_TO_FAVORITES = 'ADD_TO_FAVORITES';
