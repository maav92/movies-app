// Movies urls

export const GET_POPULAR_MOVIES = 'movie/popular';
export const GET_MOVIE_DETAIL = (movie_id) => `movie/${movie_id}`;
export const GET_MOVIE_REVIEWS = (movie_id) => `/movie/${movie_id}/reviews`;
