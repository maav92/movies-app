import { get } from '../../api/index';
import * as actionMovies from '../constants/actionTypes';
import {
  GET_POPULAR_MOVIES,
  GET_MOVIE_DETAIL,
  GET_MOVIE_REVIEWS
} from '../constants/urls';

export const getAllMovies = () =>
  dispatch => {
    dispatch(handleLoadingMovies());
    get(GET_POPULAR_MOVIES)
      .then(response => {
        dispatch(handleAllMovies(response.data.results));
      })
      .catch(error => {
        dispatch(handleErrorMovies(error));
      });
    }

export const getMovieDetails = (movie_id) =>
  dispatch => {
    dispatch(handleLoadingMovies());
    get(GET_MOVIE_DETAIL(movie_id))
      .then(response => {
        get(GET_MOVIE_REVIEWS(movie_id))
        .then(reviews => {
          dispatch(handleSelectedMovie({
            ...response.data,
            reviews: reviews.data.results
          }));
        })
        .catch(error => {
          dispatch(handleErrorMovies(error));
        });
      })
      .catch(error => {
        dispatch(handleErrorMovies(error));
      });
    }

const handleAllMovies = (data) => {
  return {
    type: actionMovies.GET_ALL_MOVIES,
    data
  }
}

const handleSelectedMovie = (data) => {
  return {
    type: actionMovies.SELECT_MOVIE,
    data
  }
}

export const addFavoriteMovie = (data) => {
  return {
    type: actionMovies.ADD_TO_FAVORITES,
    data
  }
}
    
const handleLoadingMovies = () => {
  return {
    type: actionMovies.LOADING_MOVIES
  }
}

const handleErrorMovies = (error) => {
  return {
    type: actionMovies.ERROR_MOVIES,
    error
  }
}
