import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import moviesReducer from './modules/movies';

export default function configureStore(history) {

    const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
   
    // middleware
    let middleware = [thunk];
    const routerMW = routerMiddleware(history);

    middleware = [...middleware, routerMW];

    // create store
    const store = createStore(
        combineReducers({
            movies: moviesReducer
        }),
        composeEnhancer(
            applyMiddleware(...middleware)
        )
    );
    return store;
}
