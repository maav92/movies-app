/* eslint-disable no-undef */
import * as actionMovies from '../constants/actionTypes';

const initialState = {
  allMovies: [],
  selectedMovie: false,
  favoritesMovies: [],
  loading: false,
  error: false
};

export default function MoviesReducer(state = initialState, action = {}){
	switch (action.type) {
    case actionMovies.GET_ALL_MOVIES:
      return {
        ...state,
        loading: false,
        allMovies: action.data,
        selectedMovie: false       
      };
    case actionMovies.SELECT_MOVIE:
      var exist = false;
      [...state.favoritesMovies].forEach(e => {
        if(e.id === action.data.id) exist = true;
      });

      return {
        ...state,
        selectedMovie: {
          ...action.data,
          isFavorite: exist
        },
        loading: false
      };
    case actionMovies.ADD_TO_FAVORITES:
      var newFavorites = [...state.favoritesMovies].concat(action.data);

      return {
        ...state,
        loading: false,
        selectedMovie: {
          ...state.selectedMovie,
          isFavorite: true
        },
        favoritesMovies: [...new Set(newFavorites)]
      };
    case actionMovies.ERROR_MOVIES:
      return {
        ...state,
        error: action.error,
        loading: false        
      };
    case actionMovies.LOADING_MOVIES:
      return {
        ...state,
        loading: true       
      };
    default:
      return state;
  }
}
