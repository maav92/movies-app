import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { getAllMovies } from '../reducers/actions/movies';
import MoviesGridComponent from '../components/home/moviesGrid';
import '../styles/favorites/_index.scss';
import BackIcon from '../components/icons/back-arrow';

class FavoriteMovies extends Component {
  render() {
    const { favoritesMovies } = this.props;
    return (
      <div className="FavoritesContainer">
      <Link to="/" className="go-back-link">
        <BackIcon /> Go Back
      </Link>
      <h1>My Favorites Movies</h1>
        {
          favoritesMovies.length
            ? <MoviesGridComponent movies={favoritesMovies} />
            : <h3 className="emptyContent">
              You need to add some movies :(
            </h3>
        }
      </div>
    )
  }
}

FavoriteMovies.propTypes = {
  favoritesMovies: PropTypes.array,
};

const mapStateToProps = (state) => {
  const { movies } = state;
  return movies;
}

const actions = { getAllMovies }

export default withRouter(connect(mapStateToProps, actions)(FavoriteMovies));

