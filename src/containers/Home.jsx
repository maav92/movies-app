import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getAllMovies } from '../reducers/actions/movies';
import MoviesGridComponent from '../components/home/moviesGrid';
import ErrorMessage from '../components/common/errorMessage';
import LoadingMessage from '../components/common/loadingMessage';
import '../styles/home/_index.scss';

class Home extends Component {
  componentDidMount() {
    this.props.getAllMovies();
  }

  showContent = () => {
    const { allMovies, loading, error } = this.props;
    
    if(error) {
      return <ErrorMessage error={error} />
    } else if(loading){
      return <LoadingMessage />
    } else if(allMovies && allMovies.length) {
      return <MoviesGridComponent movies={allMovies} />
    }
  }

  render() {
    return (
      <div className="HomeContainer">
        <h1>Movie Store - Populars Movies</h1>
        <Link to="/favorites" className="favorites-link">
          My Favorites Movies
        </Link>
        {this.showContent()}
      </div>
    )
  }
}

Home.propTypes = {
  getAllMovies: PropTypes.func,
  allMovies: PropTypes.array,
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
};

const mapStateToProps = (state) => {
  const { movies } = state;
  return movies;
}

const actions = { getAllMovies }

export default withRouter(connect(mapStateToProps, actions)(Home));

