import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getMovieDetails, addFavoriteMovie } from '../reducers/actions/movies';
import MovieDetailComponent from '../components/movieDetail/movieSpec';
import ErrorMessage from '../components/common/errorMessage';
import LoadingMessage from '../components/common/loadingMessage';
import MovieReviewComponents from '../components/movieDetail/movieReviews';

class MovieDetails extends Component {
  componentDidMount() {
    const { getMovieDetails } = this.props;
    // eslint-disable-next-line react/prop-types
    getMovieDetails(this.props.match.params.id);
  }

  showContent = () => {
    const { selectedMovie, loading, error } = this.props;
    
    if(error) {
      return <ErrorMessage error={error} />
    } else if(loading){
      return <LoadingMessage />
    } else if(selectedMovie && selectedMovie.id) {
      return <Fragment>
        <MovieDetailComponent {...selectedMovie} addFavoriteMovie={this.handleAddSelectedMovie} />
        <MovieReviewComponents reviews={selectedMovie.reviews} />
      </Fragment>
    }
  }

  handleAddSelectedMovie = () => {
    const { selectedMovie, addFavoriteMovie } = this.props;
    addFavoriteMovie(selectedMovie);
  }

  render() {
    return (
      <div className="MovieDetailContainer">
        {this.showContent()}
      </div>
    )
  }
}

MovieDetails.propTypes = {
  getMovieDetails: PropTypes.func,
  movies: PropTypes.object,
  selectedMovie: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  addFavoriteMovie: PropTypes.func
};

const mapStateToProps = (state) => {
  const { movies } = state;
  return movies;
}

const mapDispatchToProps = dispatch => {
  return {
      getMovieDetails: (id) => dispatch(getMovieDetails(id)),
      addFavoriteMovie: (movie) => dispatch(addFavoriteMovie(movie))
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MovieDetails));
