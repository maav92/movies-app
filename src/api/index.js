/* eslint-disable no-undef */
import axios from 'axios';
import handleError from './handleError';

const instance = axios.create({
  baseURL: `${process.env.REACT_APP_MOVIES_BASE_URL}`
});

export const get = (path) => {
  return new Promise((resolve, reject) => {
    instance.get(`${path}?api_key=${process.env.REACT_APP_MOVIES_API_KEY}`)
      .then(response => { resolve(response); })
      .catch(error => { reject(handleError(error)); });
  });
};

export default instance;
