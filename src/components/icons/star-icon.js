import React from 'react';

const StarLogo = () => (
  <svg width="19px" height="18px" viewBox="0 0 19 18">
      <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
          <g id="baseline-star_rate-18px">
              <polygon id="Path" fill="#FFA500" fillRule="nonzero" points="9.5 13.773913 15.3741667 18 13.1258333 11.1756522 19 7.04347826 11.7958333 7.04347826 9.5 0 7.20416667 7.04347826 0 7.04347826 5.87416667 11.1756522 3.62583333 18"></polygon>
              <polygon id="Path" points="0 1 18 1 18 19 0 19"></polygon>
          </g>
      </g>
  </svg>
)

export default StarLogo;
