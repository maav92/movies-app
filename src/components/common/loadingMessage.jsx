import React from 'react';

const LoadingMessage = () => (
  <div className="loadingMessage">
    <h2>Loading...</h2>
  </div>
)

export default LoadingMessage;
