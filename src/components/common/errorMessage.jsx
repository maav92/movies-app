import React from 'react';
import PropTypes from 'prop-types';

const ErrorMessage = ({ error }) => (
  <div>
    <h2>{error}</h2>
  </div>
)

ErrorMessage.propTypes = {
  error: PropTypes.string
}

export default ErrorMessage;
