import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import React from 'react';
import StarIcon from '../icons/star-icon';
import '../../styles/components/_movieCard.scss';

// eslint-disable-next-line no-undef
const img_url = `${process.env.REACT_APP_MOVIES_IMAGE_URL_SMALL}`

const MovieCardComponent = ({ title, id, poster_path, overview, release_date, vote_average }) => (
  <div className="movieCardComponent">
    <div className="poster">
      <img src={`${img_url}/${poster_path}`} alt="poster-img" />
    </div>
    <div className="detail">
      <span className="score">
        <StarIcon /> {vote_average}
      </span>

      <h3>{title}</h3>
      <small className="date">{release_date}</small>

      <p>{overview}</p>
      <Link to={`/movie/${id}`} className="see-more">See More</Link>
    </div>
  </div>
)

MovieCardComponent.propTypes = {
  title: PropTypes.string,
  id: PropTypes.string,
  poster_path: PropTypes.string,
  overview: PropTypes.string,
  release_date: PropTypes.string,
  vote_average: PropTypes.string
};

export default MovieCardComponent;
