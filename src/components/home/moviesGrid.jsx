import PropTypes from 'prop-types';
import React from 'react';
import MovieCardComponent from './movieCard';
import '../../styles/components/_moviesGrid.scss';

const MoviesGridComponent = ({ movies }) => (
  <div className="moviesGridComponent">
    {
      movies.map(item => (
        <MovieCardComponent
          key={item.id}
          {...item}
        />
      ))
    }
  </div>
)

MoviesGridComponent.propTypes = {
  movies: PropTypes.array
};

export default MoviesGridComponent;
