import React from 'react';
import PropTypes from 'prop-types';
import '../../styles/components/_movieReview.scss';

const MovieReviewComponents = ({ reviews }) => (
  <div className="movieReviewComponent">
    <h2>Latest Reviews ({reviews.length})</h2>
    {
      reviews.map(review => (
        <div key={review.id} className="review-box">
          <h3>{review.author}</h3>
          <p>{review.content}</p>
        </div>
      ))
    }
  </div>
)

MovieReviewComponents.propTypes = {
  reviews: PropTypes.array
};

export default MovieReviewComponents;