import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import StarIcon from '../icons/star-icon';
import BackIcon from '../icons/back-arrow';
import '../../styles/components/_movieDetail.scss';

// eslint-disable-next-line no-undef
const img_url = `${process.env.REACT_APP_MOVIES_IMAGE_URL_BIG}`

const MovieDetailComponent = ({ isFavorite, addFavoriteMovie, original_title, homepage, vote_count, runtime, poster_path, original_language, overview, release_date, vote_average }) => (
  <div className="movieDetailComponent">
    <div className="poster">
      <Link to="/" className="go-back-link">
        <BackIcon /> Go Back
      </Link>
      <img src={`${img_url}/${poster_path}`} alt="poster-img" />
    </div>
    <div className="detail">
      <h1>{original_title}</h1>
      
      <span className="score">
        <StarIcon /> {vote_average}
      </span>

      {
        isFavorite
          ? <span className="favorite-tag">Favorite!</span>
          : <button onClick={() => addFavoriteMovie()}>
          Add to Favorites
        </button> 
      }

      <h2>Overview </h2>
      <p>{overview}</p><br />

      <h2>Aditional Infomation</h2>

      <div className="movie-detail">
        <strong>Release date: </strong>
        <span>{release_date}</span>
      </div>

      <div className="movie-detail">
        <strong>Home Page: </strong>
        <a href={homepage} target="_blank" rel="noopener noreferrer">{homepage}</a>
      </div>

      <div className="movie-detail">
        <strong>Original Language: </strong>
        <span>{original_language}</span>
      </div>

      <div className="movie-detail">
        <strong>Runtime: </strong>
        <span>{runtime} min</span>
      </div>

      <div className="movie-detail">
        <strong>Votes: </strong>
        <span>{vote_count}</span>
      </div>
    </div>
  </div>
)

MovieDetailComponent.propTypes = {
  original_title: PropTypes.string,
  id: PropTypes.number,
  poster_path: PropTypes.string,
  overview: PropTypes.string,
  release_date: PropTypes.string,
  vote_average: PropTypes.number,
  homepage: PropTypes.string,
  vote_count: PropTypes.number,
  runtime: PropTypes.number,
  original_language: PropTypes.string,
  addFavoriteMovie: PropTypes.func,
  isFavorite: PropTypes.bool
};

export default MovieDetailComponent;
