import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router-dom';
import { createBrowserHistory as createHistory } from 'history'
import configureStore from './reducers/configureStore';
import Home from './containers/Home';
import MovieDetail from './containers/MovieDetail';
import FavoriteMovies from './containers/FavoriteMovies';
import './styles/App.scss';

const history = createHistory();

class App extends Component {
  render() {
    return (
      <Provider store={configureStore(history)}>
        <Router history={history}>
          <Route exact path="/" component={Home} />
          <Route path="/movie/:id" component ={MovieDetail} />
          <Route path="/favorites" component ={FavoriteMovies} />
        </Router>
      </Provider>
    )
  }
}

export default App;
